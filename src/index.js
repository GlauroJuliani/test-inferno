// inferno module
import Inferno from 'inferno';

// routing modules
import { Router, Route, IndexRoute } from 'inferno-router';
import createBrowserHistory from 'history/createBrowserHistory';

// app components
import MyApp from './containers/MyApp.js';
import Clock from './containers/Clock';

if (module.hot) {
	require('inferno-devtools');
}

const browserHistory = createBrowserHistory();

const routes = (
	<Router history={ browserHistory }>
		<Route path="/" component={ MyApp }/>
		<Route path="/clock" component={ Clock }/>
	</Router>
);


Inferno.render(routes, document.getElementById('app'));

if (module.hot) {
	module.hot.accept()
}

