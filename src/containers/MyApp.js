import Inferno from 'inferno';
import MyAppComponent from '../components/MyAppComponent';

export default function MyApp() {
    return (
        <div>
            <h1>Inferno Boilerplate</h1>
           <MyAppComponent />
        </div>
    );
}