import Inferno from 'inferno';
import ClockComponent from '../components/ClockComponent';

export default function Clock() {
    return (
        <div>
            <h1>Timer</h1>
            <ClockComponent />
        </div>
    );
}